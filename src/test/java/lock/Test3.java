package lock;

import util.ConvertUtil;
import util.ShardedJedisUtil;

/**
 * 5个人同时去自助机购票（synchronized）
 * 
 * @author sad
 *
 */
public class Test3 {

	public static void main(String[] args) {
		ShardedJedisUtil.set("ticketCount", String.valueOf(2)); // 设置票数（set ticketCount 2）

		BuyTicket buyTicket = new BuyTicket();
		for (int i = 0; i < 5; i++) {
			new Thread3(buyTicket).start();
		}
	}
}

class BuyTicket {
	synchronized void buy() {
		int ticketCount = ConvertUtil.intOf(ShardedJedisUtil.get("ticketCount"));
		if (ticketCount > 0) {
			ShardedJedisUtil.set("ticketCount", String.valueOf(ticketCount - 1));
			System.out.println("编号" + Thread.currentThread().getId() + "查询余票：" + ticketCount + "张，获取到ticket");
		} else {
			System.out.println("编号" + Thread.currentThread().getId() + "没有获取到ticket");
		}
	}
}

class Thread3 extends Thread {
	private BuyTicket buyTicket;

	Thread3(BuyTicket buyTicket) {
		this.buyTicket = buyTicket;
	}

	public void run() {
		this.buyTicket.buy();
	}

}
