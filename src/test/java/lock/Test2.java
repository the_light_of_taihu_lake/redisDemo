package lock;

import util.ConvertUtil;
import util.ShardedJedisUtil;

/**
 * 5个人同时去自助机购票（多线程）
 * 
 * @author sad
 *
 */
public class Test2 {

	public static void main(String[] args) {
		// ShardedJedisUtil.set("ticketCount", String.valueOf(2)); // 设置票数（set ticketCount 2）

		for (int i = 0; i < 5; i++) {
			new Thread2("序号" + i).start();
		}
	}
}

class Thread2 extends Thread {
	private String name;

	public Thread2(String name) {
		this.name = name;
	};

	public void run() {
		int ticketCount = ConvertUtil.intOf(ShardedJedisUtil.get("ticketCount"));
		if (ticketCount > 0) {
			ShardedJedisUtil.set("ticketCount", String.valueOf(ticketCount - 1));
			System.out.println(this.name + "查询余票：" + ticketCount + "张，获取到ticket");
		} else {
			System.out.println(this.name + "没有获取到ticket");
		}
	}
}
