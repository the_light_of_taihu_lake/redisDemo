package lock;

import util.ConvertUtil;
import util.ShardedJedisUtil;

public class Test5 {

	public static void main(String[] args) {
		ShardedJedisUtil.set("ticketCount", String.valueOf(5)); // 设置票数（set ticketCount 2）

		for (int i = 0; i < 20; i++) {
			new Thread5().start();
		}
	}
}

class Thread5 extends Thread {
	public void run() {
		// setnx，在指定的 key 不存在时，为 key 设置指定的值。设置成功返回1，设置失败返回0。
		boolean getLock = ShardedJedisUtil.lock("lock");
		if (!getLock) {
			return;
		}

		int ticketCount = ConvertUtil.intOf(ShardedJedisUtil.get("ticketCount"));
		if (ticketCount > 0) {
			ShardedJedisUtil.set("ticketCount", String.valueOf(ticketCount - 1));
			System.out.println("编号" + Thread.currentThread().getId() + "查询余票：" + ticketCount + "张，获取到ticket");
		} else {
			System.out.println("编号" + Thread.currentThread().getId() + "没有获取到ticket");
		}

		ShardedJedisUtil.unlock("lock");
	}
}