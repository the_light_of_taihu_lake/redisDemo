package pubsub;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.ShardedJedis;
import util.ShardedJedisUtil;

public class SubTest {

	public static final String channel = "news";

	public static void main(String[] args) {
		new MyThread("张三").start();
		new MyThread("李四").start();
	}

	static class MyThread extends Thread {
		private String name;

		public MyThread(String name) {
			this.name = name;
		};

		@SuppressWarnings("deprecation")
		public void run() {
			ShardedJedis shardedJedis = null;
			try {
				shardedJedis = ShardedJedisUtil.getShardedJedisPool().getResource();
				Jedis jedis = shardedJedis.getAllShards().toArray(new Jedis[] {})[0];
				jedis.subscribe(new RedisMsgSubListener(this.name), channel);
			} catch (Exception e) {
				try {
					ShardedJedisUtil.getShardedJedisPool().returnBrokenResource(shardedJedis);
				} catch (Exception e2) {
					e2.printStackTrace();
				}

				// 延迟
				try {
					Thread.sleep(5000);
				} catch (Exception e2) {
					e2.printStackTrace();
				}

				// 重新订阅
				this.run();
			} finally {
				ShardedJedisUtil.getShardedJedisPool().returnResource(shardedJedis);
			}

		}
	}
}
