package queue;

import org.apache.commons.lang3.StringUtils;

import util.ConvertUtil;
import util.ShardedJedisUtil;

public class Test2 {

	public static void main(String[] args) throws InterruptedException {
		ShardedJedisUtil.set("ticketCount", String.valueOf(5)); // 设置票数（set ticketCount 5）

		while (true) {
			String name = ShardedJedisUtil.rpop("queue");
			if (StringUtils.isNotBlank(name)) {
				int ticketCount = ConvertUtil.intOf(ShardedJedisUtil.get("ticketCount"));
				if (ticketCount > 0) {
					System.out.println(name + "已获取到ticket,当前余票数量：" + (ticketCount - 1));
					ShardedJedisUtil.set("ticketCount", String.valueOf(ticketCount - 1));
				} else {
					System.out.println(name + "未获取获取到ticket");
				}
			} else {
				System.out.println("---------没有新消息");
			}
			Thread.sleep(1000);
		}
	}
}
